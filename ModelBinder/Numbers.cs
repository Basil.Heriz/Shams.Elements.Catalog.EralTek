﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Shams.Elements.Catalog.EralTek.ModelBinder
{
    public static class Numbers
    {

        public static double GetDoubleNumber(this string value)
        {
            String numberDecimalSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            try
            {
                if (string.IsNullOrEmpty(value))
                    return 0;

                value = value.Replace("\t", "");
                value = value.Trim();

                value = value.GetProperNumberString();
                value = value.CorrectNumberString();

                double resultValue = 0;
                return double.TryParse(value.Replace(".", numberDecimalSeparator).Replace(",", numberDecimalSeparator), out resultValue) ? resultValue : 0;
            }
            catch
            {
                return 0;
            }

        }


        public static decimal GetDecimalNumber(this string value)
        {
            String numberDecimalSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            try
            {
                if (string.IsNullOrEmpty(value))
                    return 0;

                value = value.Replace("\t", "");
                value = value.Trim();

                value = value.GetProperNumberString();
                value = value.CorrectNumberString();

                decimal resultValue = 0;
                return decimal.TryParse(value.Replace(".", numberDecimalSeparator).Replace(",", numberDecimalSeparator), out resultValue) ? resultValue : 0;
            }
            catch
            {
                return 0;
            }

        }


        public static string GetProperNumberString(this string value)
        {

            try
            {
                value = value.Trim();
                value = Regex.Replace(value, @"[^-?\d+\.\,]", "");
            }
            catch
            {

            }

            return value;
        }

        public static string CorrectNumberString(this string value)
        {

            int characterCount = 0;
            string result = String.Empty;
            for (int i = value.Length - 1; i >= 0; i--)
            {
                if (Char.IsDigit(value, i))
                {
                    result = value[i] + result;
                }
                else
                {
                    if (i == 0 && value[i] == '-')
                        result = value[i] + result;
                    if (characterCount == 0)
                        result = value[i] + result;
                    characterCount++;
                }
            }
            return result;
        }


        public static string GetDigitsOnly(this string value)
        {
            return Regex.Replace(value, "[^.0-9]", "");
        }


    }
}
