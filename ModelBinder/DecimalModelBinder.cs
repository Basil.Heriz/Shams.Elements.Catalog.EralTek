﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Shams.Elements.Catalog.EralTek.ModelBinder;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Shams.Erp.Common.ModelBinder
{
    public class DecimalModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            if (valueProviderResult == null)
            {
                return Task.CompletedTask;
            }

            var value = valueProviderResult.FirstValue;

            if (string.IsNullOrEmpty(value))
            {
                return Task.CompletedTask;
            }

             
            decimal myValue = value.GetDecimalNumber();

            bindingContext.Result = ModelBindingResult.Success(myValue);
            return Task.CompletedTask;
        }
    }

    
}
