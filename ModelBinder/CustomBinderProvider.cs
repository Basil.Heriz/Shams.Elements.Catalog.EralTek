﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shams.Erp.Common.ModelBinder
{
    public class CustomBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.Metadata.ModelType == typeof(decimal) || context.Metadata.ModelType == typeof(Decimal))
            {
                return new DecimalModelBinder();
            }

            if (context.Metadata.ModelType == typeof(double) || context.Metadata.ModelType == typeof(Double))
            {
                return new DoubleModelBinder();
            }

            return null;
        }
    }
}
