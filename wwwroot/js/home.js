﻿var HomePage = function () {
    var brandsSelector;
    var pageIndex = 0;
    return {

        initOnce: function () {
            brandsSelector = new Choices('#brand',
                {
                    searchEnabled: true,
                    shouldSort: false,
                    placeholder: "Marka",
                    noResultsText: "Bulunamadı",
                    itemSelectText:"Seç"
                });

            $.ajax({

                url: "/Home/GetBrands",
                data: "",
                type: "POST",
                success: function (data) {
                    var brands = [];
                    brands.push({ value: "", label: "Tüm Markaları", selected: true})
                    $.each(data.Response, function (i, brand) {

                        brands.push({ value: brand.Id, label: brand.Title})
                        
                    })

                   
                    brandsSelector.setChoices(
                        brands ,
                        'value',
                        'label',
                        true,
                    );


                }
            });
            $("#search").off("keypress").on('keypress', function (e) {
                if (e.which == 13) {
                    $("#btn-search").click();
                }
            });
            $("#btn-search").off("click").click(function () {
                pageIndex = 0;
                block($("#search-result"))
                $.ajax({

                    url: "/Home/SearchProducts",
                    data: {
                        PageIndex: pageIndex,
                        Parameter: {
                            BrandId: $("#brand").val(),
                            Search : $("#search").val()
                        }
                    },
                    type: "POST",
                    success: function (data) {
                        $("#search-result").html("");
                        unblock($("#search-result"))
                        $.each(data.Response, function (i, product) {
                            var html = $("#tmp-product").render(product);
                            $("#search-result").append(html);
                        })
                        initImage();
                        HomePage.initScroll();
                    }
                });
            });
        },
        initScroll: function () {
            $(window).scroll(function () {
                if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                    pageIndex++;
                    $.ajax({

                        url: "/Home/SearchProducts",
                        data: {
                            PageIndex: pageIndex,
                            Parameter: {
                                BrandId: $("#brand").val(),
                                Search: $("#search").val()
                            }
                        },
                        type: "POST",
                        success: function (data) {
                            
                            unblock($("#search-result"))
                            $.each(data.Response, function (i, product) {
                                var html = $("#tmp-product").render(product);
                                $("#search-result").append(html);
                            })
                            initImage();
                        }
                    });
                }
            });
        }

    };
}();

$(document).ready(function () {
    HomePage.initOnce();
    initImage();
});
