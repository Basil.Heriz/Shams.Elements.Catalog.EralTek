﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Elements.Catalog.EralTek.Models.Domain
{
    public class Category
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Path { get; set; }
        public string PathWithoutMainCategory { get; set; }

        public Guid? ParentId { get; set; }
        public Guid? MainCategoryId { get; set; }

    }
}
