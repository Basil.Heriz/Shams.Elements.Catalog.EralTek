﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Elements.Catalog.EralTek.Models.Domain
{
    public class ProductExtraInfo
    {
        public Guid ProductExtraInfoId { get; set; }
        public Guid ProductId { get; set; }
        public Product Product { get; set; }
        public int ProductExtraInfoType { get; set; }
        public string Value { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
        public string Data { get; set; }
    }
}
