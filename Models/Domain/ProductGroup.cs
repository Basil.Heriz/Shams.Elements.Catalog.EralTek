﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Elements.Catalog.EralTek.Models.Domain
{
    public class ProductGroup
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public int GroupId { get; set; }
        public Product Product { get; set; }
        public Group Group { get; set; }

        public string GroupTitle { get; set; }
        public string Value { get; set; }
    }
}
