﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Elements.Catalog.EralTek.Models.Domain
{
    public class Unit
    {
        public Guid Id { get; set; }
        public string Title { get; set; } 
    }
}
