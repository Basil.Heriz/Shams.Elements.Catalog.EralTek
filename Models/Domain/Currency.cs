﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Elements.Catalog.EralTek.Models.Domain
{
    public class Currency
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public bool IsDefault { get; set; }

    }
}
