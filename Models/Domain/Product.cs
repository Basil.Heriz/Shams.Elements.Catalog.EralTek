﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Elements.Catalog.EralTek.Models.Domain
{
    public class Product
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Title2 { get; set; }
        public string SubTitle { get; set; }
        public string Code { get; set; }
        public string CategoryName { get; set; }
        public string ManufacturerCode { get; set; }
        public Guid? CategoryId { get; set; }
        public Guid? BrandId { get; set; }
        public int CurrencyId { get; set; }
        public Guid UnitId { get; set; }
        public string BrandName { get; set; }
        public double Price { get; set; }
        
        public string UnitName { get; set; }
        public int Status { get; set; }
        public double Volume { get; set; }
        public double TaxRate { get; set; }
        public string MainImage { get; set; }
        public DateTime CreateionTime { get; set; }
        public DateTime LastUpdateTime { get; set; }

        public string SearchText { get; set; }

        public List<ProductCode> ProductCodes { get; set; }
        public Brand Brand { get; set; }
        public Category Category { get; set; }
        public Unit Unit { get; set; }
        public Currency Currency { get; set; }
        public ProductDescription Description { get; set; }
        public List<ProductImage> ProductImages { get; set; }
        public List<ProductSpec> ProductSpecs { get; set; }

        public List<ProductGroup> ProductGroups { get; set; }

        public List<ProductExtraInfo> ProductExtraInfos { get; set; }
    }
}
