﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Elements.Catalog.EralTek.Models.Domain
{
    public class ProductDescription
    {
        public Guid Id { get; set; }
        public string Html { get; set; }
        public Guid ProductId { get; set; }
        public Product Product { get; set; }
    }
}
