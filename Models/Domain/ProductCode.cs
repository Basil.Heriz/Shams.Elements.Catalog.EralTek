﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Elements.Catalog.EralTek.Models.Domain
{
    public class ProductCode
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public string Code { get; set; }
        public string Type { get; set; }

        public Product Product { get; set; }
    }
}
