﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Elements.Catalog.EralTek.Models.Domain
{
    public class RelatedProductCategoryBrand
    {
        public Guid RelatedProductCategoryBrandId { get; set; }
        public Guid ProductId { get; set; }
        public Guid CategoryId { get; set; }
        public Guid BrandId { get; set; }
    }
}
