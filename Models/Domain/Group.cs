﻿using System;

namespace Shams.Elements.Catalog.EralTek.Models.Domain
{
    public class Group
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
