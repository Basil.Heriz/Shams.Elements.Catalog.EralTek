﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Elements.Catalog.EralTek.Models.Dto
{
    public class SearchProduct
    {
        public Guid BrandId { get; set; }
        public string Search { get; set; }
        public string Title { get; set; }
        public Guid CategoryId { get; set; }
        public Guid MainCategoryId { get; set; }
        public int GroupId { get; set; }
        public int ProductStatus { get; set; }

    }
}
