﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shams.Elements.Catalog.EralTek.Models;
using Shams.Elements.Catalog.EralTek.Models.Dto;
using Shams.Erp.Common.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Elements.Catalog.EralTek.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly Services.ICatalogService _catalogService;
        public HomeController(ILogger<HomeController> logger, Services.ICatalogService catalogService)
        {
            _logger = logger;
            _catalogService = catalogService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> SearchProducts(Query<SearchProduct> query)
        {

            if (query == null)
            {
                query = new Query<SearchProduct>(new SearchProduct
                {
                    ProductStatus = 1
                });
            }

            if (query.Parameter == null)
            {
                query.Parameter = new SearchProduct
                {
                    ProductStatus = 1
                };
            }
            query.Parameter.ProductStatus = 1;
            var result = await _catalogService.SearchProducts(query);
            return Ok(result);
        }


        public async Task<IActionResult> GetBrands()
        {
            var getResult = await _catalogService.GetBrands();
            return Ok(getResult);
        }

        public async Task<IActionResult> Urun(Guid id)
        {
            var getResult = await _catalogService.GetProduct(id);
            return View(getResult.Response);
        }
    }
}
