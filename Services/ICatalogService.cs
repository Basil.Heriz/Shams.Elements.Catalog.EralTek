﻿
using Shams.Elements.Catalog.EralTek.Models.Domain;
using Shams.Elements.Catalog.EralTek.Models.Dto;
using Shams.Erp.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shams.Elements.Catalog.EralTek.Services
{
    public interface ICatalogService
    { 
        Task<PaggingOperationResult<Brand>> GetBrands();
        Task<PaggingOperationResult<Category>> GetCategories();

        Task<PaggingOperationResult<Product>> SearchProducts(Query<SearchProduct> query);
        Task<OperationResult<Product>> GetProduct(Guid id);

        Task<PaggingOperationResult<RelatedProductCategoryBrand>> SearchRelatedCategoryAndBrands(Query<SearchProduct> query);
    }
}
