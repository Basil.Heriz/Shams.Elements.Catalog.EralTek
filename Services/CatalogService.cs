﻿using Microsoft.Extensions.Caching.Memory;
using Shams.Elements.Catalog.EralTek.Models.Domain;
using Shams.Elements.Catalog.EralTek.Models.Dto;
using Shams.Erp.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Shams.Elements.Catalog.EralTek.Services
{
    public class CatalogService : ICatalogService
    {
        private readonly HttpClient _httpClient;
        private readonly IMemoryCache _cash;

        public CatalogService(HttpClient client, IMemoryCache cash)
        {
            _httpClient = client;
            _cash = cash;
        }

        public async Task<PaggingOperationResult<Brand>> GetBrands()
        {
            PaggingOperationResult<Brand> result = new PaggingOperationResult<Brand>();
            try
            {


                var response = await _httpClient.GetAsync("Product/Brands");

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<PaggingOperationResult<Brand>>();


            }
            catch (Exception ex)
            {
                result.ErrorCode = "Exception";
                result.SysErrorMessage = ex.Message;
            }

            return result;
        }

        public async Task<PaggingOperationResult<Category>> GetCategories()
        {
            PaggingOperationResult<Category> result = new PaggingOperationResult<Category>();
            try
            {

                var response = await _httpClient.GetAsync("Product/Categories");

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<PaggingOperationResult<Category>>();

            }
            catch (Exception ex)
            {
                result.ErrorCode = "Exception";
                result.SysErrorMessage = ex.Message;
            }

            return result;
        }

        public async Task<OperationResult<Product>> GetProduct(Guid id)
        {
            OperationResult<Product> result = new OperationResult<Product>();
            try
            {

                var response = await _httpClient.GetAsync($"Product/Detail?id={id}");

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<OperationResult<Product>>();

            }
            catch (Exception ex)
            {
                //TODO: Log
            }

            return result;
        }

        public async Task<PaggingOperationResult<Product>> SearchProducts(Query<SearchProduct> query)
        {
            PaggingOperationResult<Product> result = new PaggingOperationResult<Product>();
            try
            {

                var response = await _httpClient.PostAsJsonAsync("Product/Search", query);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<PaggingOperationResult<Product>>();

            }
            catch (Exception ex)
            {
                result.ErrorCode = "Exception";
                result.SysErrorMessage = ex.Message;
            }

            return result;
        }

        public async Task<PaggingOperationResult<RelatedProductCategoryBrand>> SearchRelatedCategoryAndBrands(Query<SearchProduct> query)
        {
            PaggingOperationResult<RelatedProductCategoryBrand> result = new PaggingOperationResult<RelatedProductCategoryBrand>();
            try
            {

                var response = await _httpClient.PostAsJsonAsync("Product/Search-Related", query);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<PaggingOperationResult<RelatedProductCategoryBrand>>();

            }
            catch (Exception ex)
            {
                result.ErrorCode = "Exception";
                result.SysErrorMessage = ex.Message;
            }

            return result;
        }
    }
}
